This repo contains final solution of mapreduce task for printing the rows with value greater then AVG from whole set, thanks @MetaDataQ. 

I wrapped everything into excellent Spring-Hadoop integration. 

http://projects.spring.io/spring-data/

How does it work:

First job, configured in src/main/resources/applicationContext.xml Spring context file, counts the average temperature from the whole data set. Value is then saved into HDFS file. Shuffle phase of this job just sends KEY = 'combined' and VALUE = sum:count issued from every mapper in the HDFS cluster, with help from MapReduce combiner. So the amount of data sended over the network should be fine. 

Second job, which starts eventually, then grabs the value from the first job result file and prints the temperatures. Since this job uses TotalSort Big Data pattern, we're allowed to stop printing values when something isn't bigger then AVG value.

To run this: Just launch the App class.

regards

Tomas