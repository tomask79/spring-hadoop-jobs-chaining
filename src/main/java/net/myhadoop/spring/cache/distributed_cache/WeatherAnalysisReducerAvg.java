package net.myhadoop.spring.cache.distributed_cache;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WeatherAnalysisReducerAvg extends Reducer<Text, Text, Text, Text>{
	
	int count = 0;
	float sum = 0f;
	float avg = 0f;
 
	
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {
    	for (Text text: values) {
    		String arr[] = text.toString().split(":");
    		System.out.println("Reducer :"+arr[0]);
    		count = count + Integer.parseInt(arr[1]);
    		sum = sum + Integer.parseInt(arr[0]);
    	}
    	
    	avg = sum / count;
    	System.out.println("Counted avg: "+avg);
    }
    
    protected void cleanup(Context context) 
    		throws IOException, InterruptedException {
    	context.write(new Text("avg"), new Text(""+avg));
    }
}