package net.myhadoop.spring.cache.distributed_cache;

import org.apache.hadoop.mapreduce.Job;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.hadoop.mapreduce.JobRunner;

public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	ApplicationContext ctx = 
    			new ClassPathXmlApplicationContext("applicationContext.xml");
    	
    	/**
    	 * Run first job which counts avg temperatures.
    	 */
    	final JobRunner jobRunner1 = (JobRunner) ctx.getBean("avgCounter");
    	jobRunner1.call();
    	
    	/**
    	 * Start print result job
    	 */
    	final JobRunner jobRunner2 = (JobRunner) ctx.getBean("avgOutputJobRunner");
    	jobRunner2.call();
    	
    	System.out.println("************* JOBS DONE ****************");
    }
}
