package net.myhadoop.spring.cache.scaleabledump;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WeatherAnalysisReducer extends Reducer<LongWritable, Text, Text, LongWritable> {
	
	float avgFromFile = 0f;
	
	protected void setup(final Context context) {
		try {
			final BufferedReader br = getAvgBufferedReader(context);
            String line = br.readLine();
            avgFromFile = Float.valueOf(line.split(",")[1]);
            br.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	 
    public void reduce(LongWritable key, Iterable<Text> values, Context context) 
      throws IOException, InterruptedException { 
    	for(Text val : values) {
    		Integer temperature = Integer.valueOf(key.toString());
    		if (temperature > avgFromFile) {
                context.write(new Text(key + "," + val), null);
    		} else { 
    			// we don't need to iterate anymore, 
    			// since we used TotalSort big data pattern.
    			break;
    		}
        }
    }
    
    /**
     * Get buffered reader to HDFS file where first job left us result.
     * Parameter 'avg.hdfs.filepath' is set in Spring framework.
     * 
     * @param ctx
     * @return
     * @throws Exception
     */
    private BufferedReader getAvgBufferedReader(final Context ctx) throws Exception {
    	Path pt=new Path(ctx.getConfiguration().get("avg.hdfs.filepath"));
        FileSystem fs = FileSystem.get(ctx.getConfiguration());
        BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(pt)));
        
        return br;
    }
}

